// alert('hi!)
// 3
const num = 2;
const getCube = num ** 3;
// 4
console.log(`The cube of ${num} is ${getCube}`);
// 5
const address = [258, 'Washington Ave NW', 'California', 90011];
// 6
const [streetNumber, streetName, state, zipCode] = address;
console.log(`I live at ${streetNumber} ${streetName}, ${state} ${zipCode}`);
// 7
const animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: '1075 kgs',
	length: '20 ft 3 in'
};
// 8
const {name, type, weight, length} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${length}.`);
// 9
let numberArray = [1, 2, 3, 4, 5];
// 10
numberArray.forEach((numbers) => {
	console.log(`${numbers}`)
});
// 11
const reduceNumber = (previousValue, currentValue) => previousValue + currentValue;
console.log(numberArray.reduce(reduceNumber));
// 12
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
// 13
const myDog = new Dog('Lewk', 1, 'Chihuahua');
console.log(myDog);
